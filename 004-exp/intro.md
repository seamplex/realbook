The examples in this section show how a single ordinary differential equation can be solved with wasora. Indeed this is one of its main features, namely the ability to solve systems of differential-algebraic equations written as natural algebraic expressions. In particular, the equation the examples solve is

$$ \frac{dx}{dt} = -x $$


with the initial condition $x_0 = 1$, which has the trivial analytical solution $x(t) = e^{-t}$.
