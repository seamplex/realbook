changequote([,])
So, after the tailor-made joke, we thus proceed to solve the simple pendulum using the three discussed approaches. In order to do that, we prepare a main input file that defines the parameters of the problem and writes the output, but includes the filename indicated in the commandline containing the actual equations to solve. The three options of included files are:

Newton’s equations in `newton.was`, reduced from a single second-order differential equation to two first-order equations:

~~~wasora
esyscmd([cat newton.was])
~~~

Lagrange equations in `lagrange.was`, computed as numerical derivatives using dummy variables of the algebraic Lagrangian written as a wasora function:

~~~wasora
esyscmd([cat lagrange.was])
~~~

Hamilton equations in `hamilton.was`, idem as in `lagrange.was`:

~~~wasora
esyscmd([cat hamilton.was])
~~~


The main input is called `pendulum.was`, and the formulation used to solve the problem is selected from the commandline using the replacement mechanism of the construction `$1` in the wasora input file:

