
So, after the tailor-made joke, we thus proceed to solve the simple pendulum using the three discussed approaches. In order to do that, we prepare a main input file that defines the parameters of the problem and writes the output, but includes the filename indicated in the commandline containing the actual equations to solve. The three options of included files are:

Newton’s equations in `newton.was`, reduced from a single second-order differential equation to two first-order equations:

~~~wasora
PHASE_SPACE theta omega

0 .= m*l*omega_dot + m*g*sin(theta)
theta_dot .= omega

~~~

Lagrange equations in `lagrange.was`, computed as numerical derivatives using dummy variables of the algebraic Lagrangian written as a wasora function:

~~~wasora
# lagrangian of the simple pendulum
L(theta,thetadot) := 1/2*m*(l*thetadot)^2 + m*g*l*cos(theta)

# lagrange equations
PHASE_SPACE theta dL_dthetadot 
VAR theta' theta_dot'

dL_dthetadot     .= derivative(L(theta, theta_dot'), theta_dot', theta_dot)
dL_dthetadot_dot .= derivative(L(theta', theta_dot), theta', theta)

~~~

Hamilton equations in `hamilton.was`, idem as in `lagrange.was`:

~~~wasora
# hamiltonian of the simple pendulum
H(theta,p_theta) := p_theta^2/(2*m*l^2) - m*g*l*cos(theta) 

# hamilton equations
PHASE_SPACE theta p_theta
VAR theta' p_theta'

theta_dot    .= +derivative(H(theta, p_theta'), p_theta', p_theta)
p_theta_dot  .= -derivative(H(theta', p_theta), theta', theta)

~~~


The main input is called `pendulum.was`, and the formulation used to solve the problem is selected from the commandline using the replacement mechanism of the construction `$1` in the wasora input file:

