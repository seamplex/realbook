This is a tale about mixing [GSL](http://www.gnu.org/software/gsl)  and [SUNDIALS](https://computation.llnl.gov/casc/sundials/). It continues on the next chapter.



![Newton, Lagrange and Hamilton](guys.jpg){.img-responsive}

Once upon a time there were three guys named Isaac, Joseph-Louis and William, respectively. Suddenly, they found a bob of mass $m$ hanging from a string of length $\ell$ in presence of a gravitational field of intensity $g$. They could see a certain angle $\theta$ between the string and the vertical. They all agreed that $\theta=0$ was taken when the bob was at the bottommost position and $\theta>0$ int the counter-clockwise direction.

![Simple pendulum](simple.svg){.img-responsive}

The first one said, “I think 

$$ m \ell \ddot{\theta} + m g \sin\theta = 0 $$

and as my friend Galileo told me, it can be seen the mass $m$ does not matter as it can be taken out of the expression without affecting $\dot{\theta}$ .” “Let me see if it matters or not”---replied the second one. “It seems to me that

$$ \frac{d}{dt} \left( \frac{\partial \mathcal{L}}{\partial \dot{\theta}} \right) = \frac{\partial \mathcal{L}}{\partial \theta} $$

where

$$ \mathcal{L}(\theta, \dot{\theta}) = \frac{1}{2} m (\ell \dot{\theta})^2 + mg\ell \cos\theta $$

and apparently $m$ appears in both sides, so I agree with Isaac that it should not matter after all.” The third one said “I like your idea, but what about if

$$
\begin{align*}
\dot{\theta}   &= +\frac{\partial \mathcal{H}}{\partial p_\theta} \\
\dot{p}_\theta &= -\frac{\partial \mathcal{H}}{\partial \theta}
\end{align*}
$$

where now

$$ \mathcal{H}(\theta, p_\theta) = \frac{p_\theta^2}{2m\ell^2} - mg\ell \cos\theta $$

Mmmmmm. How can you two tell me $m$ does not matter?”

