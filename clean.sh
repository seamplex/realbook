rm -rf html

for example in 0*; do

 echo $example
 cd $example

 if [ -e clean.sh ]; then
  ./clean.sh
 fi
 rm -f *.png *.pdf tmp
 rm -f *~


 for inputorig in `cat inputs`; do
   saltear=`echo $inputorig | grep \~ | wc -l`
   echo $inputorig $saltear

   if [ $saltear -eq 0 ]; then
    rm -f $inputorig.term
   fi
   
 done
 cd ..
done
