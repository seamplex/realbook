The output this input generates is directly fed to the script `qdp` (Quick & Dirty Plot) which generates a plot of the angular speed $\omega$ vs. time. Is resemblance with the Lorenz attractor discussed in section ref{005-lorenz} is remarkable.

A full analysis of the behavior of this system with respect to the control parameter $Q$ can be done with wasora using the `PARAMETRIC` keyword and analyzing the output with different algorithms. However, this kind of study is beyond the scope of this "wasora Real Book": "http://www.talador.com.ar/jeremy/wasora/realbook".

