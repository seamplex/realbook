This time, instead of having wasora to actually solve the equations whilst besssugo draws a graphical representation of the wheel, we first ask wasora to solve the system, dump the solution into a file and then load it with wasora+besssugo and perform a constant time-step graphical representation of the loaded data.

Instead of including the input file `wheel.was` we use the following input called `readmap.was` to read a text file provided in the commandline (which should be the ouput of `wheel.was`), interpolate it  and leave it ready (i.e. map it) for `drawing.was` to understand it:

@@@CODE 018-waterwheel/readmap.was envir=wasora

Thefore, the new case use the same drawing keywords as the previous one but uses a constant time step (which is wasora's default equal to $1/16$).


