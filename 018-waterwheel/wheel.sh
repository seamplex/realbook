wasora wheel.was | qdp -o wheel-dat --xlabel "\$t\$ [s]" --ylabel "\$\\omega\$ [rad/s]" --ycol 2 --ti "\$\\omega\$"
