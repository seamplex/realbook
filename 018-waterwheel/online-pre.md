So far with the equations and the plots. The main objective of the waterwheel is fun, and there cannot be fun without drawings. So here enters the wasora plugin *besssugo*, which provides means to draw graphical representations based on wasora's objects.  First of all, we have to make sure besssugo is correctly compiled and installed (i.e. the shared object file `besssugo.so` is located in a directory where the operating system can find it such as `/usr/local/lib` or `$LD_LIBRARY_PATH`):

!bc console
$ wasora -p besssugo.so
besssugo 0.2.1 trunk (2014-01-03 19:48:41 -0300 dirty)
scientific video generator for wasora
$
!ec

In this section we use besssugo at the same time we ask wasora to solve the equations, so we draw the wheel and the buckets at each step. First of all, we write a file called `drawing.was` (which we will reuse afterwads):

@@@CODE 018-waterwheel/drawing.was envir=wasora

Then we use the following file `online.was` as the main wasora input file:

