I first read about the Lorenzian waterwheel in James Gleick's "Chaos: Making a New Science" back in 2004 when I was an undergraduate student. Some weeks after that, I built a physical version of the waterwheel at the University, measured its behavior, compared it to numerical results and prepared a poster about it (presented of course at the classical poster session of Física Experimental I at "Instituto Balseiro": "http://www.ib.edu.ar"):

FIGURE: [018-waterwheel/jeremywheel] Me, myself and my experimental waterwheel in 2004 label{jeremywheel}

FIGURE: [018-waterwheel/poster] The resulting "poster": "http://www.talador.com.ar/jeremy/papers/2004-rueda.pdf"

The chaotic waterwheel has somehow fascinated me ever since, and I certainly returned to its study in 2006/2007 during the writing of my "engineering thesis": "http://www.talador.com.ar/jeremy/papers/2007-theler-grado-1sided.pdf" where in one of the chapters I apply a fuzzy logic controller to force the (numerical) wheel to rotate always in the same direction.  Indeed, non-linear dynamics of nuclear reactors was the subject of my "masters thesis": "http://www.talador.com.ar/jeremy/papers/2008-theler-maestria-1sided.pdf", which was chosen after my delightful experience with chaotic systems.


This report shows how "wasora": "http://www.talador.com.ar/jeremy/wasora" can help us building a numerical model of the Lorenzian chaotic waterwheel by just writing the differential equations of movement into an input file just as nearly we would write them in a sheet of paper, and let the DAE-solving libraries to take care of all the numerical details. In particular, this example focuses on a numerical waterwheel model that tries to resemble the experimental version I built almost ten years ago, which in turn is based on the description Gleick gives in his book. Other variations of waterwheels---both numerical and experimental---can be found online.


The Lorenzian waterwheel consists of an array of $N$ cylindrical buckets of height $h$ and diameter $b$ equally spaced around a disc that is able to rotate around an axis, configuring a circle or radius $R$. The particularity is that the buckets have a little hole at their bottom so water is drained at a certain rate. Water is poured from above at a volumetric rate $Q$ by means of a faucet or a hose that is located exactly at the center of the wheel. When the buckets are empty, the wheel has a momentum of inertia $I_0$ and the bearing around which it rotates provides a frictional damping.

FIGURE: [018-waterwheel/wheel3d] A 3D model of the experimental wheel 

If the water flow $Q$ is small, the buckets quickly empty and due to the damping in the bearing, the wheel eventually stops rotating. However, if the flow is high enough, due to the nonlinear nature of the system, the wheel may rotate in either direction and present what it is known as *chaos*, i.e. non-periodic long-term behavior and sensitivity to initial-conditions. Several studies show that this system can be related to the seminal Lorenz' system (see the wasora example in section ref{005-lorenz}), whereas in this case the water flow $Q$ acts as the control parameter $r$ of the original system.

MOVIE: [realwheel.webm, width=320 height=240] The real chaotic waterwheel I built in action


The equations that describe the wheel's movement are easily derived as follows. Of course, the angular acceleration $\dot{\omega}$ is the ratio of the net instantaneous torque $\tau$ and the instantaneous momentum of inertia $I$:

!bt
\[ \dot{\omega} = \frac{\tau}{I} \]
!et

The angular position $\theta$ of the wheel is defined as the location of bucket number one, defining $\theta = 0$ as the vertical position above the wheel bearing and taking the positive direction as the clockwise direction. Let $v_i$ be the instantaneous of water contained in bucket $i$. Therefore

!bt
\begin{align*}
I &= I_0 + \rho R^2  \cdot \sum_{i=1}^{N} v_i \\
\tau &= -k \cdot \omega + \rho g R \cdot \sum_{i=1}^{N} v_i \cdot \sin \left( \theta + (i-1) \cdot \frac{2\pi}{N} \right)
\end{align*}
!et

where of course $\rho$ is the water density and $g$ is the gravity acceleration. It should be noted that the equation above for the torque does not include the change of momentum between the water falling down the source and the bucket, which depending on the experimental configuration may not be negligible.

Now we have to give the rates at which the buckets are filled and emptied, which is in fact where fun part is located. On the one hand, the $N$ buckets continually drain water at at rate that in principle is proportional to the square root of the water level through a constant $p$. On the other hand, a certain bucket $i$ is filled with water at the rate $Q$ only when its diameter $b$ is located beneath the flow up to the arc subtended by the head of the bucket. Using the angular coordinate $\theta$, this condition can be written as $\theta + (i-1)2\pi/N$ modulo $2\pi$ is either less that $\gamma = \arctan(\frac{b}{2R})$ or greater than $2\pi - \gamma$ (make a drawing to get this approximation straight). However, as the buckets have a limited capacity, they cannot hold more than a certain maximum volume $v_\text{max} = \pi b^2 h/4$. The rate of change of the volume contained in the $N$ buckets is therefore

!bt
\[
 \dot{v}_i =
 -p \sqrt{\frac{4 v_i}{\pi b^2}} \text{ if } v_i>0 
 + Q \text{ if } v_i < v_\text{max} \wedge
  \left[ \theta + (i-1) \frac{2\pi}{N} \bmod 2\pi < \gamma  \vee
         \theta + (i-1) \frac{2\pi}{N} \bmod 2\pi > 2\pi-\gamma \right]
\]
!et

Summing up all the equations, we can arrive at a system of algebraic-differential equations of the form $\mathbf{F}(\mathbf{\dot{x}}, \mathbf{x}, t) = 0$ of size $4+N$ over the phase-space coordinates $\theta$, $\omega$, $I$, $\tau$ and $v_i$ for $i=1,\dots, N$ that can be easily handled by wasora:

!bt
\begin{align*}
\dot{\theta} &= \omega \\
\dot{\omega} &= \frac{\tau}{I} \\
I &= I_0 + \rho R^2  \cdot \sum_{i=1}^{N} v_i \\
\tau &= -k \cdot \omega + \rho g R \cdot \sum_{i=1}^{N} v_i \cdot \sin \left( \theta + (i-1) \cdot \frac{2\pi}{N} \right) \\
 \dot{v}_i &= -p \sqrt{\frac{4 v_i}{\pi b^2}} \text{ if } v_i>0 
 + Q \text{ if } v_i < v_\text{max} \wedge
  \left[ \theta + (i-1) \frac{2\pi}{N} \bmod 2\pi < \gamma  \vee
         \theta + (i-1) \frac{2\pi}{N} \bmod 2\pi > 2\pi-\gamma \right]
\end{align*}
!et 

