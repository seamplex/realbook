FIGURE: [025-mathace/mathace-niederreiter] The Math Ace solved with a $256 \times 256$ `niederreiter` sequence

FIGURE: [025-mathace/mathace-sobol] The Math Ace solved with a $256 \times 256$ `sobol` sequence

FIGURE: [025-mathace/mathace-halton] The Math Ace solved with a $256 \times 256$ `halton` sequence

FIGURE: [025-mathace/mathace-reversehalton] The Math Ace solved with a $256 \times 256$ `reversehalton` sequence

FIGURE: [025-mathace/mathace-reversehalton2] The Math Ace solved with a $512 \times 512$ `reversehalton` sequence, starting from the $256\times256$ solution

FIGURE: [025-mathace/mathace-reversehalton3] The Math Ace solved with a $768 \times 768$ `reversehalton` sequence, starting from both the $256\times256$ and $512\times512$ solutions

FIGURE: [025-mathace/mathace-reversehalton-fine] The Math Ace solved with a $768 \times 768$ `reversehalton` sequence, starting from scratch


Note that the running time is fairly high due to the fact that wasora's sometime-to-be-replaced-algebraic parser and evaluator does not work in a tree-based way as proposed by Knuth in is "LR Parser": "http://en.wikipedia.org/wiki/LR_parser" algorithm of 1965, but in a easier-to-write-but-unefficient multilevel array organization.
