set tics scale 0.2
set xrange [-13:13]
set yrange [-18:18]
unset key
set size square
set terminal png size 720,720

set output "mathace-niederreiter.png"
plot "mathace-niederreiter.dat"   pt 0 lc rgb "#FF0000"
set output "mathace-sobol.png"
plot "mathace-sobol.dat"          pt 0 lc rgb "#660000"
set output "mathace-halton.png"
plot "mathace-halton.dat"         pt 0 lc rgb "#990000"
set output "mathace-reversehalton.png"
plot "mathace-reversehalton.dat"  pt 0 lc rgb "#330000"
set output "mathace-reversehalton2.png"
plot "mathace-reversehalton.dat"  pt 0 lc rgb "#330000",\
     "mathace-reversehalton2.dat" pt 0 lc rgb "#FF9999"
set output "mathace-reversehalton3.png"
plot "mathace-reversehalton.dat"  pt 0 lc rgb "#330000",\
     "mathace-reversehalton2.dat" pt 0 lc rgb "#FF9999",\
     "mathace-reversehalton3.dat" pt 0 lc rgb "#CCCCCC"
set output "mathace-reversehalton-fine.png"
plot "mathace-reversehalton-fine.dat"  pt 0 lc rgb "#CCCCCC"
