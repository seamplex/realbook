But for me, the pursuit of money has always played second fiddle to

the pursuit of beauty. Thank goodness for mathematics and

computer science, whose methods offer many ways by which our left

brains and our right brains can both be happy, simultaneously.

*Donald. E. Knuth, Selected Papers on Fun & Games, 2011*


Mankind's primary concern is beauty. Call it love, art or science. Whatever. "Donald E. Knuth": "http://en.wikipedia.org/wiki/Donald_Knuth" knows it. And it is himself that shows us one of the most aesthetically pleasant examples of mathematical and computer graphics I have ever seen. Back in 1959 he asked its readers to predict what a certain equation meant. Please try to figure out what the result is before turning the page or scrolling down. Believe me, it is worth the shot:

!bt
 \begin{align*}                               
\left(                                        
 \Big|                                        
  \big|                                       
   3-|x|                                      
  \big| - 3 + |x|                             
 \Big| +                                      
 \bigg|                                       
  \Big|                                       
   \sqrt{ |9-x^2| } -                         
   \big| y-2/3 |x|                            
   \big|                                      
  \Big| - \sqrt{|9-x^2|} +                    
  \big| y - 2/3 |x|                           
  \big|                                       
 \bigg|                                       
\right) \quad \\                              
\Bigg[                                        
 \big| |x y|  + xy \big|                      
 +  \left(                                    
\bigg|                                        
 \Big| 2 -                                    
  \big|33 - 3 |x|                             
  \big|                                       
 \Big|                                        
 - 2 +                                        
 \big| 33 - 3 |x|                             
 \big|                                        
\bigg| +                                      
  \big| 14 - |y|                              
  \big|                                       
\right)                                       
  \quad\quad \\                               
  \left( \Big| 16 - |y| - 3 \big|11 - |x| \big| \Big|  
  \Bigg| \bigg|  \sqrt{\big| 1 - (11 - |x|)^2 \big|} - \Big| 11 - |y| + 2/3 \big|11 - |x|\big| \Big| \bigg| \right. \quad\quad\quad \\ 
        \left. - \sqrt{\big| 1 - (11 - |x|)^2 \big|} + \Big| 11 - |y| + 2/3 \big|11 - |x|\big| \Big| \Bigg| \right. \quad\quad\quad \\ 
 \left. + \bigg|\Big| 1 - \big|11 - |x|\big| \Big| - 1 + \big|11 - |x|\big|\bigg| \right) \Bigg] 
&= 0 
\end{align*}
!et

The absolute value bars may be dazzling, but the "wasora": "http://www.talador.com.ar/jeremy/wasora" input below shows the actual implementation of the left hand side of the equation as a function $f(x,y)$ where the arguments to the `abs` function are clearly enclosed in parentheses. If this is not love, art and science, I wonder what we are doing in this tiny planet.

