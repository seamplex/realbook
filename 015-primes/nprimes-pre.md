The last example of the section loads the routine `nprimes()` which counts how many prime numbers less or equal than $n$ there are. As with the previous example, a wasora routine is  defined which can be again used in any context where a function could be used. In this case, the value of `nprimes(n)` is compared to two approximations of the number of prime numbers less or equal than $n$, namely

$$
\pi_1(n) \approx \frac{n}{\ln n}
$$

$$
\pi_2(n) \approx \int_2^n \frac{dt}{\ln t}
$$


