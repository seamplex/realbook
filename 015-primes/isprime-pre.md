We now load the routine `isprime()` that again takes a single argument but now it returns either a one or a zero. Therefore, we can define a wasora function `isprime(n)` of a single variable and use it as a regular wasora function. Note that the argument $n$ is rounded to the nearest integer before checking wether it is prime or not.

The example prints the wasora function `isprime(n)` for $n=1,2,\dots,25$ with `PRINT_FUNCTION`, and after that it evaluates the function at the very same points the previous example did with `checkprime`. Again, see `prime.c` for details of the implementation.
