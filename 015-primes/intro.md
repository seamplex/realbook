
Sometimes, the features provided by wasora are not enough to perform a certain computation. This example shows how wasora can be extended by using arbitrary snippets of code by dynamically loading a shared object file. The user-provided routines can be programmed in whatever language you may feel is suitable for the particular computation, as long as it is able to produce position-independent objects which could be linked into a dynamic library.

The examples below use C functions to test whether a certain number $n$ is prime or not. First, a file called `prime.c` is prepared with the following contents:

~~~c
#include <stdio.h>
#include <math.h>

int isprimeint(int);
double isprime(const double *);
double nprimes(const double *);
double checkprime(const double *);

// check if an integer n is prime or not
int isprimeint(int n) {

  int i;
  int sqrtn;
  int nmod6;

  if (n <= 1) {
    return 0;  // negative numbers, zero and one are not primes
  } else if (n == 2 || n == 3) {
    return 1;  // two and tree are primes
  }

  // any other prime is of the form 6k+1 or 6k-1 (prove it)
  nmod6 = n % 6;
  if (nmod6 == 1 || nmod6 == 5) {
    // check for canditate divisors from 2 up to sqrt(n)+1
    sqrtn = sqrt(n) + 1;
    for (i = 2; i < sqrtn; i++) {
      if (n % i == 0) {
        return 0;
      }
    }
    // none found, so n is prime
    return 1;
  } else {
    return 0;
  }

}


// wasora-like routine to be called without checking the return value
double checkprime(const double *x) {
  int n = (int)(round(*x));
  printf("%d is%s prime\n", n, isprimeint(n)?"":" not");
  return 0;
}

// wasora-like function that returns true or false
double isprime(const double *x) {
  return isprimeint((int)round(*x));
}

// wasora-like routine that return the number of primes
// smaller or equal than n
double nprimes(const double *x) {

  int n = (int)(round(*x));
  int np = 0;
  int i;

  for (i = 0; i < n; i++) {
    np += isprimeint(i);
  }

  return (double)np;

}

~~~

A dynamic library called `libprime.so` can be generated with

~~~
$ gcc -c prime.c -fPIC
$ gcc -shared -o libprime.so prime.o
~~~

And then the individual routines can be either called or used as functions from within wasora.


