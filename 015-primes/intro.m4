changequote([,])
Sometimes, the features provided by wasora are not enough to perform a certain computation. This example shows how wasora can be extended by using arbitrary snippets of code by dynamically loading a shared object file. The user-provided routines can be programmed in whatever language you may feel is suitable for the particular computation, as long as it is able to produce position-independent objects which could be linked into a dynamic library.

The examples below use C functions to test whether a certain number $n$ is prime or not. First, a file called `prime.c` is prepared with the following contents:

~~~c
esyscmd([cat prime.c])
~~~

A dynamic library called `libprime.so` can be generated with

~~~
$ gcc -c prime.c -fPIC
$ gcc -shared -o libprime.so prime.o
~~~

And then the individual routines can be either called or used as functions from within wasora.


