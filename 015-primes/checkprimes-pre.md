This example dynamically loads the the routine `checkprime()` from the shared object `libprime.so` and uses the keyword `CALL` to execute it, using a single argument. Remember that wasora works only with double-precision arithmetich, and so the loaded (and called) routines need as a prototype:

~~~ c
double routine(double *x);
~~~

If the routine does not take any argument, `NULL` will be passed. It is the user's responsibility to make sure the routine reads the very same number of arguments as passed from the wasora input. In this case, the function `checkprime()` takes one argument, which wasora evaluates and then passes it to the routine as a pointer to an array of doubles. When calling a routine, the return value is discarded, so the result of the call is directly printed to the standard output by the routine itself (see the source `prime.c` above).


