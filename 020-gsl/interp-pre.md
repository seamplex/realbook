The following program demonstrates the use of the interpolation and
spline functions.  It computes a cubic spline interpolation of the
10-point dataset $(x_i, y_i)$ where $x_i = i + \sin(i)/2$ and
$y_i = i + \cos(i^2)$ for $i = 0 \dots 9$.

To keep the original spirit, the data is plotted using GNU ploutils `graph` progam.

The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/interp.c

The corresponding wasora input: