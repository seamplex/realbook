gcc -o nlfit nlfit.c -lgslcblas -lgsl -lm
./nlfit | tee gsl.out
cat gsl.out | grep data > experimental.dat
wasora nlfit.was
pyxplot nlfit.ppl
