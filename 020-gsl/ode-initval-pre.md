The following program solves the second-order nonlinear Van der Pol
oscillator equation

!bt
\[
u''(t) + \mu u'(t) (u(t)^2 - 1) + u(t) = 0
\]
!et

This can be converted into a first order system suitable for use with
the routines described in this chapter by introducing a separate
variable for the velocity, $v = u'(t)$,

!bt
\begin{align*}
u' &= v\cr
v' &= -u + \mu v (1-u^2)
\end{align*}
!et

The program begins by defining functions for these derivatives and
their Jacobian. The main function uses driver level functions to solve
the problem. The program evolves the solution from $(u, v) = (1,0)$
at $t=0$ to $t=100$.  The step-size $h$ is
automatically adjusted by the controller to maintain an absolute
accuracy of $10^{-6}$ in the function values $(u, v)$.  
The loop in the example prints the solution at the points
$t_i = 1, 2, \dots, 100$.


It is important to note that wasora does not use GSL's ordinary
differential equations solvers because wasora works with DAEs
instead of ODEs (which of course are a particular case of DAEs).
The solver is based on the SUNDIALS library IDA. Nevertheless,
dynamical systems are one of the main subjects of wasora so
this example is particularly relevant. The special variable
`rel_error` is set to $10^{-6}$ as in GSL and both the variables
`min_dt` and `max_dt` are set to one, so as to reproduce the 
solution only at the points $t_i=1,2,\dots,100$.

The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/ode-initval.c

The corresponding wasora input:
