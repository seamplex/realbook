gcc -o interp interp.c -lgslcblas -lgsl -lm
./interp > interpgsl.dat
head interpgsl.dat
tail interpgsl.dat
graph -Tpng -C -L GSL < interpgsl.dat > interpgsl.png
wasora interp.was > interpwas.dat
head interpwas.dat
tail interpwas.dat
graph -Tpng -C -L wasora < interpwas.dat > interpwas.png
