This example estimates the derivative of the function $f(x) = x^{3/2}$ at $x=2$ and at $x=0$. The function $f(x)$ is undefined for $x<0$ so the derivative at $x=0$ is computed using `gsl_deriv_forward` in the C example. To reproduce this behavior in wasora, a positive value $p>0$ is passed as the optional fifth argument (see the `derivative` functional documentation in the wasora reference). Again, wasora does not provide a way to estimate the error.


The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/diff.c

The corresponding wasora input:
