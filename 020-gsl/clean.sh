rm -f *~ tmp
rm -f *.svg
rm -f data* *.dat
rm -f gsl.log
rm -f *.pdf gsl.tex
rm -f gsl.out 

for i in `cat inputs`; do

  rm -f $i
  rm -f $i.term

done