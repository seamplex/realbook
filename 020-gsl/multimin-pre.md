This example program finds the minimum of a paraboloid function.
The location of the minimum is offset from the origin
in $x$ and $y$, and the function value at the minimum is
non-zero. The initial step-size is chosen as 0.01, a conservative estimate in this
case, and the line minimization parameter is set at 0.0001.  The program
terminates when the norm of the gradient has been reduced below 0.001.

In wasora, as the minimization function is algebraic, the gradient
can be entered easily as shown. If it was not entered, it would be
automatically computed by finite differences (new versions of GSL
do the same). 

The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/multimin.c

The corresponding wasora input:
