Quasi-random sequences are used by wasora to perform parametric studies. The idea is to sweep a certain portion of the parameter space by choosing test points, which can be linear (type `LINEAR`), logarithmic (`LOGARITHMIC`), uniformly chosen at random (`RANDOM`), randomly distributed using a gaussian distribution (`GAUSSIANRANDOM`) or follow a certain quasi-random sequence (types `SOBOL`, `NIEDERREITER`, `HALTON` and `REVERSEHALTON`). By defining a two-dimensional window $[0,1]\times[0,1]$ and just printing the chosen parameters, the GSL example can be reproduced in wasora.


The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/stat.c

The corresponding wasora input:
