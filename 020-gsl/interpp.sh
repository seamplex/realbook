gcc -o interpp interpp.c -lgslcblas -lgsl -lm
./interpp > interppgsl.dat
graph -Tpng -C -L GSL < interppgsl.dat > interppgsl.png
wasora interpp.was > interppwas.dat
graph -Tpng -C -L wasora < interppwas.dat > interppwas.png
