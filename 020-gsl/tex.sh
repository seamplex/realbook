out=gsl.tex
echo > $out

for i in `cat inputs`; do

  echo $i

  if [ -e $i-pre.tex ]; then
   cat $i-pre.tex >> $out
#    echo "\\input{$i-pre.tex}" > $out
  fi
  echo >> $out

  if [ -e $i.c ]; then
    echo "\begin{lstlisting}[style=C,title=$i.c]" >> $out
    cat $i.c >> $out
    echo "\end{lstlisting}" >> $out
    echo "\medskip" >> $out
    echo >> $out
  fi

  if [ -e $i.was ]; then
    echo "\begin{lstlisting}[style=wasora,title=$i.was]" >> $out
    cat $i.was >> $out
    echo "\end{lstlisting}" >> $out
    echo "\medskip" >> $out
    echo >> $out
  fi

  if [ -e $i.moch ]; then
    echo "\begin{lstlisting}[style=wasora,title=$i.moch]" >> $out
    cat $i.moch >> $out
    echo "\end{lstlisting}" >> $out
    echo "\medskip" >> $out
    echo >> $out
  fi

  if [ -e "$i.sh" ]; then
    rm -f $i.term

    j=1
    n=`wc -l < $i.sh`
    while [ $j -le $n ]; do
      cat $i.sh | head -n $j | tail -n1 > tmp #| read command arg1 arg2 arg3 arg4 arg5 arg6
      command=`cat tmp | awk '{print $1}'`
      arg1=`cat tmp | awk '{print $2}'`
      arg2=`cat tmp | awk '{print $3}'`
      arg3=`cat tmp | awk '{print $4}'`
      arg4=`cat tmp | awk '{print $5}'`
      arg5=`cat tmp | awk '{print $6}'`
      arg6=`cat tmp | awk '{print $7}'`
      arg7=`cat tmp | awk '{print $8}'`
      arg8=`cat tmp | awk '{print $9}'`
      arg9=`cat tmp | awk '{print $10}'`
      echo "\$" $command $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 >> $i.term
      if [ "$arg1" = ">" ]; then
        ($command > $arg2) >> $i.term
      elif [ "$arg2" = ">" ]; then
        ($command $arg1 > $arg3) >> $i.term
      elif [ "$arg3" = ">" ]; then
        ($command $arg1 $arg2 > $arg4) >> $i.term
      elif [ "$arg4" = ">" ]; then
       ($command $arg1 $arg2 $arg3 > $arg5) >> $i.term
      elif [ "$arg4" = ">>" ]; then
        ($command $arg1 $arg2 $arg3 >> $arg5) >> $i.term
      elif [ "$arg5" = "<" -a "$arg7" = ">" ]; then
        ($command $arg1 $arg2 $arg3 $arg4 < $arg6 > $arg8) >> $i.term
      else
        $command $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 >> $i.term
      fi
      j=`echo $j + 1 | bc`
    done
    echo "\$" >> $i.term

    echo >> $out
    echo "\begin{lstlisting}[style=bash]" >> $out
    cat $i.term >> $out
    echo "\end{lstlisting}" >> $out
    echo "\medskip" >> $out
    echo >> $out
  fi

  if [ -e $i-post.tex ]; then
    cat $i-post.tex >> $out
#  echo "\\input{$i-post.tex}" >> $out
  fi

done
