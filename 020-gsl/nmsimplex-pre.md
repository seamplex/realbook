Here is another example using the Nelder-Mead Simplex algorithm to
minimize the same example object function, as above.

The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/nmsimplex.c

The corresponding wasora input:
