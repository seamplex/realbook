The next program demonstrates a periodic cubic spline with 4 data
points.  Note that the first and last points must be supplied with 
the same $y$-value for a periodic spline.

The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/interpp.c

The corresponding wasora input: