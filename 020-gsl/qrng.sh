gcc -o qrng qrng.c -lgslcblas -lgsl -lm
./qrng > qrng-gsl.dat
wasora qrng.was | tee qrng-was.dat | graph -Tpng -C -m -3 -S 2 > qrng.png
diff -s qrng-gsl.dat qrng-was.dat
