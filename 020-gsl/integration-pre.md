Consider the following integral, which has an
algebraic-logarithmic singularity at the original


!bt
\[
\int_0^1 x^{-1/2} \log(x) \, dx = 4
\]
!et

The original example uses the `QAGS` integrator, which is not implemented in wasora. However, the regular `integral` functional (which corresponds to the `QAG` integrator) does an acceptable job. Currently, wasora does not provide a way to report the estimated errors.

The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/integration.c

The corresponding wasora input:
