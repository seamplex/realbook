This example uses the function solver `gsl_root_fsolver_brent`
for Brent's method to solve the following equation

!bt
\[
x^2 - 5 = 0
\]
!et

with solution $x = \sqrt{5} = 2.236068\dots$. First, the function to be
solved is defined in the following two files `demo_fn.h` and `demo_fn.c`:

@@@CODE 020-gsl/demo_fn.h

@@@CODE 020-gsl/demo_fn.c



The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/roots.c

The corresponding wasora input:
