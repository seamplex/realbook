The following program demonstrates the use of a random number generator
to produce uniform random numbers in the range $[0.0, 1.0)$. In the GSL version,
The numbers depend on the seed used by the generator.  The default seed
can be changed with the `GSL_RNG_SEED` environment variable to
produce a different stream of numbers.  The generator itself can be
changed using the environment variable `GSL_RNG_TYPE`. In wasora, the generator
is fixed to the `gsl_rng_knuthran2002` method, basically because the author
looks up at Knuth as the most important figure of modern computer science.
Therefore, to obtain two identical sequences of numbers both with GSL and wasora, we run both examples with the same generator and seed.

The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/rngunif.c

The corresponding wasora input:
