The following program uses the Brent algorithm to find the minimum of
the function $f(x) = \cos(x) + 1$, which occurs at $x = \pi$.
The starting interval is $[0,6]$. The GSL version uses an initial guess for the
minimum of $x=2$. However, wasora takes the initial guess at the center of the
starting interval.


The C program as implemented in the GSL documentation:

@@@CODE 020-gsl/min.c

The corresponding wasora input:
