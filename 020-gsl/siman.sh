gcc -o siman siman.c -lgslcblas -lgsl -lm
./siman > simangsl.dat
wasora siman.was > simanwas.dat
head simangsl.dat simanwas.dat
tail simangsl.dat simanwas.dat
pyxplot siman-f.ppl
pyxplot siman-position.ppl
pyxplot siman-energy.ppl
