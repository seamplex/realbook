As seen in the terminal, the functional `func_min` performs the minimization at once and does not provide a mean to show information about intermediate steps.
