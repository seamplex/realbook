A thorough discussion of the beauty and applications of the Fibonacci sequence is of course out of both this Real Book and its author league. For sure the reader is already familiar with this set of natural numbers. The three examples that follow show how to use wasora to compute the Fibonacci numbers, first by constructing a vector of a certain size and setting each element to be equal to the sum of the two previous elements, then iteratively computing

$$ f_{n} = f_{n-1} + f_{n-2} $$

and then by using the dazzling closed-form formula

$$ f_{n} = \frac{\phi^n + (1-\phi)^n}{\sqrt{5}} $$

where $\phi= (1+\sqrt{5})/2$ is the golden ratio. As cumbersome as it may seem, this formula is easily obtained by solving the explicit difference equation, for example with the aid of the $Z$-transform:

$$ f_{n} = z f_{n} + z^2 f_{n} $$

$$ z^2 + z - 1 = 0 $$

$$ z = \frac{1 \pm \sqrt{5}}{2} $$

The general solution is therefore

$$ f_n = k_1 \left( \frac{1+\sqrt{5}}{2} \right)^n + k_2 \left(\frac{1-\sqrt{5}}{2} \right)^n $$

where $k_1$ and $k_2$ should be determined as to fulfill the initial conditions $f_1 = 1$ and $f_2=1$. The solution is

$$
\begin{align*}
 k_1 &= +\frac{1}{\sqrt{5}}\\
 k_2&= -\frac{1}{\sqrt{5}}
\end{align*}
$$

and the closed-form formula follows.
