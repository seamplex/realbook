changequote([,])

As [here](real-012-mechanics.html), the problem formulation (i.e. the equations of motions) is selected from the commandline amongst four options:

 * Lagrangian formulation with analytical derivatives
 * Lagrangian formulation with numerical derivatives
 * Hamiltonian formulation with analytical derivatives
 * Hamiltonian formulation with numerical derivatives
 
The files that implement these formulations are:

Lagrange’s equations with the explicit analytical derivatives shown above reduced to a first-order system in `lagrange-analytical.was`:

~~~wasora
esyscmd([cat lagrange-analytical.was])
~~~

Lagrange’s equations written as numerical derivatives of $\mathcal{L}$ reduced to a first-order system in `lagrange-numerical.was`:

~~~wasora
esyscmd([cat lagrange-numerical.was])
~~~

Hamiltons’s equations with the explicit analytical derivatives shown above in `hamilton-analytical.was`:

~~~wasora
esyscmd([cat hamilton-analytical.was])
~~~

Hamilton’s equations written as numerical derivatives of $\mathcal{H}$ in `hamilton-numerical.was`:

~~~wasora
esyscmd([cat hamilton-numerical.was])
~~~

The main input is called `double.was`, which should be called with an argument stating which of the four formulations should be used to solve the double pendulum:



