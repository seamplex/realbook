This is the continuation of a tale about mixing [GSL](http://www.gnu.org/software/gsl/)  and [SUNDIALS](https://computation.llnl.gov/casc/sundials/) that started [here](real-012-mechanics.html). It shows how [wasora](https://www.seamplex.com/wasora)---which is defined as a *syntactically-sweetened way to ask a computer to perform a certain mathematical calculation*---can be used to solve Hamiltonian mechanical systems no only by just writing down the equations of motion in a text file but also without needing to do so! Just stating the Hamiltonian (or Lagrangian) of a system and them using the `derivative` functional to have wasora to numerically compute the partial derivatives (using [GSL](http://www.gnu.org/software/gsl/) and then to solve the resulting differential equations (using [SUNDIALS](https://computation.llnl.gov/casc/sundials/)). Moreover, a scientific video is generated from the computed time series with the wasora plugin [besssugo](https://www.seamplex.com/besssugo) (using [SDL](http://www.libsdl.org)), much like those found in [Wikipedia](https://en.wikipedia.org/wiki/Double_pendulum).

In this section, we solve the double pendulum that can be mentally built by attaching a massless string of length $\ell_2$ with a bob of mass $m_2$ to an existing simple pendulum (see section ref{012-mechanics}) of length $\ell_1$ and mass $m_1$. The two angles $\theta_1$ and $\theta_2$ are measured from the vertical and the positive direction is taken counter-clockwise:

![An ideal double pendlum](double.svg)

Very much like the [Lorenz system](real-005-lorenz.html), the double pendulum displays a very complex behavior despite its simple concept. Paying attention only to the Lagrangian and Hamiltonian formulations (as the derivation of the Newton equations for this case are rather cumbersome), we can write the Lagrangian as the difference $T - V$ of $m_1$ and $m_2$ kinetic and potential energies using the angles $\theta_1$ and $\theta_2$ as the generalized coordinates:

$$ \mathcal{L}(\theta_1, \theta_2, \dot{\theta}_1, \dot{\theta}_2) = 
\frac{1}{2} m_1 \ell_1^2 \dot{\theta}_1^2 +
\frac{1}{2} m_2 \left[\ell_1^2 \dot{\theta}_1^2 + \ell_2^2 \dot{\theta}_2^2 + 2 \ell_1 \ell_2 \dot{\theta}_1 \dot{\theta}_2 \cos(\theta_1-\theta_2)\right] +
 m_1 g  \ell_1\cos \theta_1 +
 m_2 g \left(\ell_1\cos \theta_1 + \ell_2 \cos \theta_2 \right)
$$

and the Hamiltonian as the sum $T+V$, written as a function of the angles $\theta_1$ and $\theta_2$ and of the generalized angular momenta $p_1 = \partial \mathcal{L}/\partial \dot{\theta}_1$ and $p_2 = \partial \mathcal{L}/\partial \dot{\theta}_2$:

$$
\mathcal{H}(\theta_1, \theta_2, p_1, p_2) =
 - \frac{\ell_2^2 m_2 p_1^2 - 2 \ell_1 \ell_2 m_2 \cos(\theta_1-\theta_2) p_1 p_2 + \ell_1^2 (m_1+m_2) p_2^2}
        {\ell_1^2 \ell_2^2 m_2 \left[-2m_1-m_2+m_2\cos\Big(2(\theta_1-\theta_2)\Big)\right]}
 - \Big[ m_1 g \ell_1 \cos \theta_1 + m_2 g (\ell_1 \cos \theta_1 + \ell_2 \cos \theta_2) \Big]
$$

After performing the tedious task of manually (and carefully) taking the analytical derivatives, we can arrive at the equations of motion. Lagrange says

$$
\begin{align*}
0 &= (m_1+m_2) \ell_1^2 \ddot{\theta}_1 + 
 m_2 \ell_1 \ell_2 \ddot{\theta}_2 \cos(\theta_1-\theta_2) + 
 m_2 \ell_1 \ell_2 \dot{\theta}_2^2 \sin(\theta_1-\theta_2) + 
 \ell_1 (m_1+m_2) g \sin(\theta_1) \\
0 &= m_2 \ell_2^2 \ddot{\theta}_2 + 
 m_2 \ell_1 \ell_2 \ddot{\theta}_1 \cos(\theta_1-\theta_2) - 
 m_2 \ell_1 \ell_2 \dot{\theta}_1^2 \sin(\theta_1-\theta_2) + 
 \ell_2 m_2 g \sin(\theta_2)
\end{align*}
$$

whilst Hamilton states that

$$
\begin{align*}
\dot{\theta}_1 &=          \frac{p_1 \ell_2 - p_2 \ell_1 \cos(\theta_1-\theta_2)}{\ell_1^2 \ell_2 [m_1 + m_2 \sin^2(\theta_1-\theta_2)]} \\
\dot{\theta}_2 &=   \frac{p_2 (m_1+m_2)/m_2 \ell_1 - p_1 \ell_2 \cos(\theta_1-\theta_2)}{\ell_1 \ell_2^2 [m_1 + m_2 \sin^2(\theta_1-\theta_2)]} \\
\dot{p_1} &= -(m_1+m_2) g \ell_1 \sin(\theta_1) - c_1 + c_2 \\
\dot{p_2} &= -m_2 g \ell_2 \sin(\theta_2) + c_1 - c_2
\end{align*}
$$

where the expressions $c_1$ and $c_2$ are

$$
\begin{align*}
c1 &= \frac{p_1 p_2 \sin(\theta_1-\theta_2)}{\ell_1 \ell_2 \Big[m_1+m_2 \sin(\theta_1-\theta_2)^2\Big]} \\
c2 &= \frac{\Big[ p_1^2 m_2 \ell_2^2 - 2 p_1 p_2 m_2 \ell_1 \ell_2 cos(\theta_1-\theta_2)
 + p_2^2 (m_1+m_2) \ell_1^2)\Big] \sin(2 (\theta_1-\theta_2)}{
 2 \ell_1^2 \ell_2^2 \left[m_1+m_2 \sin^2(\theta_1-\theta_2)\right]^2}
\end{align*}
$$

