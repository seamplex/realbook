Once we used wasora to solve the four proposed formulations of the double pendulum, we can use the plugin [besssugo](https://www.seamplex/besssugo) to generate a video which better illustrates the chaotic nature of the system. And, of course, we cannot deny that videos are far more sexy than static plots of variables versus time.

On the one hand, we know that videos usually have a fixed frames-per-second value whilst numerical integration of DAE systems have variable time steps. And, on the other hand, we want to have a single video comparing four different solutions that involve functions of time but that are sampled at different times. Therefore, it is best to load the data to be drawn into the video from the output of previous executions instead of solving again the problems at the moment of generating the video. We thus load the positions $x_1(t)$, $y_1(t)$, $x_2(t)$ and $y_2(t)$ of the centers of both bobs which are located in the sixth, seventh, eighth and ninth column of the output files generated in the previous subsection. As for sure we will have to interpolate the data because the time steps of the video in general will not coincide with the data, we choose the akima interpolation method to obtain smooth functions of time.

We name the cases as

 a. Lagrange with analytical derivatives
 b. Lagrange with numerical derivatives
 c. Hamilton with analytical derivatives
 d. Hamilton with numerical derivatives


The video is divided into a $2 \times 2$ grid showing the Lagrangian (cases $a$ & $b$) on the upper half and Hamiltonian (cases $c$ & $d$) on the lower half, with the analytical derivatives (cases $a$ and $c$) on the left half and the numerical derivatives (cases $b$ and $d$) on the right half.

The wasora+besssugo input has `dt=1/200` to have smooth trajectories. One out of five individual frames are dumped as PNG image fles, giving 40 frames per second. Then the frames are converted into a [WebM](http://www.webmproject.org) video using [avconv](http://libav.org) with 20 frames per second, resulting in a video that advances at half the real time speed so as to show with some detail what the pendulums but avoiding boring the reader with an extremely slow motion. Enjoy chaos!
