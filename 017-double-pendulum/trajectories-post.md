<div class="embed-responsive embed-responsive-16by9">
 <video width="600" height="600" controls loop>
  <source src="trajectories.webm" type="video/webm">
  Your browser does not support the video tag.
 </video> 
Trajectories of four almost-exactly-equal double pendulums at half real-time speed
</div>
