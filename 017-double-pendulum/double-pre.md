

As here(real-012-mechanics.html), the problem formulation (i.e. the equations of motions) is selected from the commandline amongst four options:

 * Lagrangian formulation with analytical derivatives
 * Lagrangian formulation with numerical derivatives
 * Hamiltonian formulation with analytical derivatives
 * Hamiltonian formulation with numerical derivatives
 
The files that implement these formulations are:

Lagrange’s equations with the explicit analytical derivatives shown above reduced to a first-order system in `lagrange-analytical.was`:

~~~wasora
# the double pendulum solved by the lagrangian formulation
# and analytically computing its derivatives

PHASE_SPACE theta1 theta2 omega1 omega2

# reduction to a first-order system
omega1 .= theta1_dot
omega2 .= theta2_dot

# lagrange equations
0 .= (m1+m2)*l1^2*omega1_dot + \
 m2*l1*l2*omega2_dot*cos(theta1-theta2) + \
 m2*l1*l2*omega2^2*sin(theta1-theta2) + \
 l1*(m1+m2)*g*sin(theta1)


0 .= m2*l2^2*omega2_dot + \
 m2*l1*l2*omega1_dot*cos(theta1-theta2) - \
 m2*l1*l2*omega1^2*sin(theta1-theta2) + \
 l2*m2*g*sin(theta2)

~~~

Lagrange’s equations written as numerical derivatives of $\mathcal{L}$ reduced to a first-order system in `lagrange-numerical.was`:

~~~wasora
# the double pendulum solved by the lagrangian formulation
# and numerically computing its derivatives

PHASE_SPACE theta1 theta2 dL_dthetadot1 dL_dthetadot2
VAR theta1' theta2' theta1_dot' theta2_dot'

L(theta1,theta2,theta1_dot,theta2_dot) := {
# kinetic energy of m1
 1/2*m1*l1^2*theta1_dot^2 + 
# kinetic energy of m2
 1/2*m2*(l1^2*theta1_dot^2 + l2^2*theta2_dot^2 + 2*l1*l2*theta1_dot*theta2_dot*cos(theta1-theta2))
 + ( 
# potential energy of m1 
 m1*g *  l1*cos(theta1) +
# potential energy of m2
 m2*g * (l1*cos(theta1) + l2*cos(theta2))
 ) }

dL_dthetadot1 .= derivative(L(theta1, theta2, theta1_dot', theta2_dot), theta1_dot', theta1_dot)
dL_dthetadot2 .= derivative(L(theta1, theta2, theta1_dot, theta2_dot'), theta2_dot', theta2_dot)

dL_dthetadot1_dot .= derivative(L(theta1', theta2,theta1_dot, theta2_dot), theta1', theta1)
dL_dthetadot2_dot .= derivative(L(theta1, theta2',theta1_dot, theta2_dot), theta2', theta2)


~~~

Hamiltons’s equations with the explicit analytical derivatives shown above in `hamilton-analytical.was`:

~~~wasora
# the double pendulum solved by the hamiltonian formulation
# and analytically computing its derivatives

PHASE_SPACE theta1 theta2 p1 p2 c1 c2

theta1_dot .=            (p1*l2 - p2*l1*cos(theta1-theta2))/(l1^2*l2*(m1 + m2*sin(theta1-theta2)^2))
theta2_dot .= (p2*(m1+m2)/m2*l1 - p1*l2*cos(theta1-theta2))/(l1*l2^2*(m1 + m2*sin(theta1-theta2)^2))

p1_dot .= -(m1+m2)*g*l1*sin(theta1) - c1 + c2
p2_dot .=      -m2*g*l2*sin(theta2) + c1 - c2

c1 .= p1*p2*sin(theta1-theta2)/(l1*l2*(m1+m2*sin(theta1-theta2)^2))
c2 .= { (p1^2*m2*l2^2 - 2*p1*p2*m2*l1*l2*cos(theta1-theta2)
         + p2^2*(m1+m2)*l1^2)*sin(2*(theta1-theta2))/
        (2*l1^2*l2^2*(m1+m2*sin(theta1-theta2)^2)^2) }

~~~

Hamilton’s equations written as numerical derivatives of $\mathcal{H}$ in `hamilton-numerical.was`:

~~~wasora
# the double pendulum solved by the hamiltonian formulation
# and numerically computing its derivatives

PHASE_SPACE theta1 theta2 p1 p2
VAR theta1' theta2' p1' p2'

H(theta1,theta2,p1,p2) := {
 - (m1*g*l1*cos(theta1) + m2*g*(l1*cos(theta1) + l2*cos(theta2)))
 - (l2^2*m2*p1^2 - 2*l1*l2*m2*cos(theta1-theta2)*p1*p2 + l1^2*(m1+m2)*p2^2)
       /(l1^2*l2^2*m2 * (-2*m1-m2+m2*cos(2*(theta1-theta2))))
}

theta1_dot .= +derivative(H(theta1,theta2,p1',p2), p1', p1)
theta2_dot .= +derivative(H(theta1,theta2,p1,p2'), p2', p2)

p1_dot     .= -derivative(H(theta1',theta2,p1,p2), theta1', theta1)
p2_dot     .= -derivative(H(theta1,theta2',p1,p2), theta2', theta2)

~~~

The main input is called `double.was`, which should be called with an argument stating which of the four formulations should be used to solve the double pendulum:



