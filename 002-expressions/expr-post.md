By default `PRINT` introduces a tab between its arguments. This behavior can be changed with the `SEPARATOR` keyword.
