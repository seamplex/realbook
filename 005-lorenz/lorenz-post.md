The ability to solve the Lorenz system---that has both intrigued and inspired me since I was old enough to understand differential equations---with such simple and concise instructions shows me that indeed wasora has something to provide to the scientific/engineering community.

See also the [besssugo plugin quickstart examples](http://www.talador.com.ar/jeremy/wasora/besssugo/#thelorenzsystem) for videos of applications of wasora to solve and study the Lorenz equations---and other chaotic dynamical systems.
