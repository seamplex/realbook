#!/bin/bash
example=`echo $1 | sed s_/__`
if [ -z "${example}" ]; then
  echo usage: $0 example
fi

dotxt="text.do.txt"
body="body.tex"

cd ${example}
title=`head -n1 meta | sed -E 's/([#$%&_\\])/\\\\&/g'`
author=`head -n2 meta | tail -n1`
escapedtags=`head -n3 meta | tail -n1 | sed -E 's/([#$%&_\\])/\\\\&/g'`
tags=`head -n3 meta | tail -n1`
difficulty=`echo $example | cut -c-3`

rm -rf pdf
hg clone ../../../latechgdoc/ pdf
cd pdf

cat << EOF > fields.m4
divert(-1)
define(\`latechg_title',           \`${title}')
define(\`latechg_subtitle',        \`An example of application of the wasora code')
define(\`latechg_type',            \`Wasora Realbook Example')
define(\`latechg_docnumber',       \`WA-WA-RB-14-0${difficulty}')
define(\`latechg_class',           \`article')
define(\`latechg_mainlanguage',    \`english')
define(\`latechg_classoptions',    \`11pt')
define(\`latechg_keywords',        \`${escapedtags}')
define(\`latechg_latexbackend',    \`pdflatex')
define(\`latechg_bibtexbackend',   \`')
divert(0)dnl
EOF

rm -rf ${dotxt}
cat << EOF >> ${dotxt}

======= ${title} =======

label{$example}

 * Difficulty: $difficulty/100
 * Author: $author
 * Keywords: \`$tags\`

EOF

if [ -e ../intro.do.txt ]; then
  cat ../intro.do.txt | sed s/${example}/\.\./ >> ${dotxt}
  echo >> ${dotxt}
fi

if [ ! -e ../inputs ]; then
  touch ../inputs
fi


for inputorig in `cat ../inputs`; do
 input=`echo $inputorig | sed s/~//`
 echo "    $inputorig"

cat << EOF >> ${dotxt}

===== ${input}.was =====

EOF

  if [ -e ../$input-pre.do.txt ]; then
    cat ../$input-pre.do.txt | sed s/${example}/\.\./ >> ${dotxt}
  fi

  echo "\lstinputlisting[style=wasora,title=$input.was]{../$input.was}" >> ${dotxt}

  if [ -e ../$input.term ]; then
    echo "\begin{lstlisting}[style=bash]" >> ${dotxt}
    cat ../$input.term | grep -v Script | sed s/^\\.\$/\$/ >> ${dotxt}
    echo "\end{lstlisting}" >> ${dotxt}
    echo "\medskip" >> ${dotxt}
   fi

  if [ -e ../$input.figs ]; then
    cat ../$input.figs | while read figure; do
      cp ../${figure}* .
      echo "FIGURE:[$figure]" >> ${dotxt}
      echo >> ${dotxt}
    done
  fi

  if [ -e ../$input-post.do.txt ]; then
    cat ../$input-post.do.txt | sed s/${example}/\.\./ >> ${dotxt}
  fi
done

doconce format pdflatex text --no_header_footer

rm -f ${body}
echo "\abstract{}" >> ${body}
echo "\maketitle" >> ${body}
m4 -Dbwasora='begin{lstlisting}[style=wasora]' \
   -Dewasora='end{lstlisting}' text.p.tex >> ${body}
echo "\label{lastpage}" >> ${body}
sed -i 's/\\code{\([^}]*\)}/\\verb!\1!/g' ${body}  

pwd=`pwd`
cd ../../../doc/
./syntax-tex.sh > ${pwd}/syntax.tex
cd ${pwd}

cat << EOF >> preamble.tex
% Movies are handled by the href package
\newenvironment{doconce:movie}{}{}
\newcounter{doconce:movie:counter}

\input{syntax}
EOF

hg branch example
hg add
hg commit -m "Automatically generated"
./latechgdoc.sh
cp WA-WA-RB-14-0${difficulty}*.pdf ../..
cd ../..
