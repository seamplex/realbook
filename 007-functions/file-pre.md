This example shows three different methods of one-dimensional interpolation provided by wasora (indeed, by the GNU Scientific Library). The data is read from a file, so the three functions use the same data set but are interpolated  in different ways. Recall that whenever a one-dimensional point-wise function called `f` is defined, three new variables are also defined:

 1. `f_a` contains the first point-wise value of the independent variable
 2. `f_b` contains the last point-wise value of the independent variable
 3. `f_n` contains the number of points of definition
