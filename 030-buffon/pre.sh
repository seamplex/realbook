for i in *.svg; do
 filename=`basename $i .svg`
 inkscape -D -e $filename.png $i
 inkscape -D -A $filename.pdf $i
done
