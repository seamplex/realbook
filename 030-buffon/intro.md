Buffon's needle is a classical probability problem that dates back from the\ XVIII century whose solution depends on the value of\ $\pi$. When I first read about this problem in my high-school years, I could not believe two things. The first one, that the number\ $\pi$ had something to do with the probability a stick has of crossing a line. And the other, that one would actually be able to compute $\pi$ by throwing away sticks. Of course, this was long before I learned about calculus, distributions and Monte Carlo methods. 

The problem consists of a table of length\ $L$ over which transversal lines separated by a length\ $d$ are drawn. A stick (needle) of length\ $\ell$ is randomly thrown over the table. What is the probability\ $p$ that the stick crosses one line?

![A table with transversal lines](needle.png){.img-responsive}\ 

For $\ell < d$ the answer is

$$ p = \frac{ 2 \ell }{ \pi d} $$

To convince myself that the two facts I did not believe back when I was a youngster were actually true, I would just run the example below. Four experiments (I know that generating random numbers in a digital computer is not a real experiment, as neither is solving the equations of a chaotic natural convection loop. However, I could not come up with a better word) of ten millions throws each are simulated, and the experimental frequency is compared to the theoretical value. I am now convinced.

