To solve the Buffon's needle problem the Monte Carlo way, two random numbers are generated: the distance $0 \leq x < L$  from one side of the table to the center of the thrown stick, and the angle $0 \leq \theta < 2\pi$ with respect to the table longitudinal axis. One way of checking  whether a stick crosses or not a line is the following. First, compute the location of both ends of the stick

\begin{align*}
 x_1 &= x + \frac{1}{2} \ell \cos(\theta)  \\
 x_2 &= x - \frac{1}{2} \ell \cos(\theta)  \\
\end{align*}

Now, if the floor of $x_1/d$ is equal to the floor of $x_2/d$, the stick does not cross a line. Otherwise, it does. 

The input file iteratively performs $10^7$ throws and prints the partial frequency of crosses as a function of the number of throws, along with the constant analytical result. Four runs are performed, and the results are plotted in the figure.
