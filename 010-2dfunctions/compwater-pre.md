This example shows an extension of the example about saturated water in section ref{007-functions} by giving properties of compressed water as a function of pressure $p$ and temperature $T$. The file `compwater.txt` contains some properties of water as a function of temperature and pressure in a rectangular grid over the pressure-temperature space. The enthalpy is not contained in the file, but it can be computed from pressure $p$, the internal energy $u(p,T)$ and the specific volume $v(p,T)$ as

!bt
\[ h(p,t) = u(p,T) + p \cdot v(p,T) \]
!et

