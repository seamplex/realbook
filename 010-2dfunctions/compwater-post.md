The figure shows the enthalpy of compressed water as a continuous function of $p$ and $T$ and the discrete experimental data.
