set ticslevel 0

set size square
unset colorbox

set xtics 1
set ytics 1
set xlabel "x"
set ylabel "y"
set zlabel "z"

set terminal pdf
set output "scattered1a.pdf"

set view 65,250

splot \
      "n_int.dat" u 1:2:3 w p pt 57 ps 0.25 palette ti "nearest",\
      "n_def.dat" w p ps 1.5 lt 3 pt 59 ti "definition points"

set terminal png
set output "scattered1a.png"

replot



set terminal pdf
set output "scattered1b.pdf"

set view 45,195
replot

set terminal png
set output "scattered1b.png"

replot




set terminal pdf
set output "scattered1c.pdf"

set view 55,100
replot

set terminal png
set output "scattered1c.png"

replot


set terminal pdf
set output "scattered2d1.pdf"

set xrange [-1:1]
set yrange [-1:1]
plot "n_int.dat" u 1:2:3 w image ti "",\
     "n_def.dat" w p ps 1.5 lt 3 pt 59 ti ""

set terminal png
set output "scattered2d1.png"

replot




# ----------------------------------
set terminal pdf
set output "scattered2a.pdf"

set view 65,250

splot \
      "n_int.dat" u 1:2:4 w p pt 57 ps 0.25 palette ti "shepard",\
      "n_def.dat" w p ps 1.5 lt 3 pt 59 ti "definition points"

set terminal png
set output "scattered2a.png"

replot



set terminal pdf
set output "scattered2b.pdf"

set view 45,195
replot

set terminal png
set output "scattered2b.png"

replot




set terminal pdf
set output "scattered2c.pdf"

set view 55,100
replot

set terminal png
set output "scattered2c.png"

replot


set terminal pdf
set output "scattered2d2.pdf"

set xrange [-1:1]
set yrange [-1:1]
plot "n_int.dat" u 1:2:4 w image ti "",\
     "n_def.dat" w p ps 1.5 lt 3 pt 59 ti ""

set terminal png
set output "scattered2d2.png"

replot

# ----------------------------------
set terminal pdf
set output "scattered3a.pdf"

set view 65,250

splot \
      "n_int.dat" u 1:2:5 w p pt 57 ps 0.25 palette ti "modified shepard",\
      "n_def.dat" w p ps 1.5 lt 3 pt 59 ti "definition points"

set terminal png
set output "scattered3a.png"

replot



set terminal pdf
set output "scattered3b.pdf"

set view 45,195
replot

set terminal png
set output "scattered3b.png"

replot




set terminal pdf
set output "scattered3c.pdf"

set view 55,100
replot

set terminal png
set output "scattered3c.png"

replot


set terminal pdf
set output "scattered2d3.pdf"

set xrange [-1:1]
set yrange [-1:1]
plot "n_int.dat" u 1:2:5 w image ti "",\
     "n_def.dat" w p ps 1.5 lt 3 pt 59 ti ""

set terminal png
set output "scattered2d3.png"

replot
