This example shows how the sum $1+1$ can be computed and printed to the standard output---i.e. the screen---in a variety of ways.
