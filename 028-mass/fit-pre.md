Given a certain nuclide characterized by the number~$Z$ of protons and the total number~$A$ of nucleons (i.e. protons plus neutrons), the "Weizsäcker's mass formula" :"http://en.wikipedia.org/wiki/Semi-empirical_mass_formula" based on the liquid drop model gives a expected dependence of the binding energy per nucleon~$B/A$ in terms of algebraic sums of powers of~$Z$ and~$A$ that involve some multiplicative factors that are to be determined experimentally.

FIGURE: [028-mass/fsm] A graphical representation of the binding energy per nucleon as a function of the mass number of the nuclide using the liquid drop model. This curve actually illustrates why both fission of heavy nuclei and fusion of light nuclei release energy.

To make a long story short, the semi-empirical mass formula says that the binding energy per nucleon of a nuclide of mass number~$A$ with atomic number~$Z$ is

!bt
\[
\frac{B}{A}(A,Z) \approx a_1 - a_2 \cdot A^{-1/3} - a_3 \cdot Z^2 A^{-4/3} - a_4 \cdot (A-2Z)^2 A^{-2} + a_5 \cdot \delta \cdot A^{-\gamma}
\]
!et

with

!bt
\[
 \delta =
\begin{cases}
+1 & \text{for even-$A$ and even-$Z$} \\
0  & \text{for odd-$A$} \\
-1 & \text{for even-$A$ and odd-$Z$} \\
\end{cases}
\]
!et

and~$a_i$ for~$i=1,\dots,5$ and~$\gamma$ are six real constants to be empirically determined.

In this case, measured data from the "2003 Atomic Mass Evaluation": "http://www.nndc.bnl.gov/masses/" is used as the experimental data. A point-wise defined function~$D(A,Z)$ is defined, reading the data from a file with three columns, namely $A$, $Z$ and $B/A$  called `binding_per_A.dat`. Another algebraic function~$W(A,Z)$ is defined following the proposed functional form and leaving the to-be-determined constants as parameters. Using the `FIT` keyword, we ask "wasora": "http://www.talador.com.ar/jeremy/wasora" (actually "GSL": "http://www.gnu.org/software/gsl/") to find the values of the constants that better fit $W(A,Z)$ to $D(A,Z)$. Using the `VERBOSE` keyword we can see how the iterative procedure advances. When a result is finally obtained, we write a file called `the14.was` that we will use later to compare our result with other fits.

FIGURE: [028-mass/experimental2d] Experimental binding energy for 2011 nuclides. Data taken from the "2003 Atomic Mass Evaluation": "http://www.nndc.bnl.gov/masses/"

FIGURE: [028-mass/experimental3d] Surface view of experimental binding energy for 2011 nuclides. Data taken from the "2003 Atomic Mass Evaluation": "http://www.nndc.bnl.gov/masses/"
