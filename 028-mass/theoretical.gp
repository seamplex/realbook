set ticslevel 0
set xlabel "mass number A"
set ylabel "atomic number Z"
set zlabel "binding energy per nucleon [MeV]" rotate

set xrange [1:268]
set yrange [1:108]
set size ratio 108.0/268.0
unset key
set tics scale 0.2
set tics nomirror
set cbrange [0:9]

set palette defined ( 0 1 1 0.4, 8 0.7 0.7 0.8, 9 0.9 0.1 0.9)

set title "theoretical binding energy per nucleon"

set terminal pdf size 16cm,9cm
set output "theoretical3d.pdf"
splot "the14.dat" u 1:2:4 palette  ps 0.5 pt 57

set terminal png size 900,500 crop
set output "theoretical3d.png"
replot

set terminal pdf size 16cm,9cm
set output "theoretical2d.pdf"
set view map
set border 2+8

replot

set terminal png size 900,450 nocrop
set output "theoretical2d.png"
replot
