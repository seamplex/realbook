The fit obtained by wasora gives the following results when $W(A,Z)$ is evaluated for the 2011 nuclides contained in the experimental set is practically indistinguishable from the actual experimental data, which are shown again:

FIGURE: [028-mass/theoretical2d] Theoretical binding energy for 2011 nuclides given by the semi-empirical mass formula fitted with wasora.

FIGURE: [028-mass/experimental2d] Experimental binding energy for 2011 nuclides. Data taken from the "2003 Atomic Mass Evaluation": "http://www.nndc.bnl.gov/masses/"

FIGURE: [028-mass/theoretical3d] Surface view of theoretical binding energy for 2011 nuclides given by the semi-empirical mass formula fitted with wasora.

FIGURE: [028-mass/experimental3d] Surface view of experimental binding energy for 2011 nuclides. Data taken from the "2003 Atomic Mass Evaluation": "http://www.nndc.bnl.gov/masses/"


To effectively compare the fit to the data, its difference is to be analyzed. The figures that follow show the difference between the experimental data and the different fits with a color scale. The greener the plot, the smaller the difference:

FIGURE: [028-mass/seg652d] Difference as computed by Segre's coefficients

FIGURE: [028-mass/coh712d] Difference as computed by Cohen's coefficients (probably uses another form of the formula)

FIGURE: [028-mass/mey672d] Difference as computed by Meyerhoff's coefficients (probably uses another form of the formula)

FIGURE: [028-mass/pea862d] Difference as computed by Pearson's coefficients

FIGURE: [028-mass/the052d] Difference as computed by me back in 2005

FIGURE: [028-mass/the142d] Difference as computed by wasora


Same information from a different point of view:

FIGURE: [028-mass/seg653d] Difference as computed by Segre's coefficients

FIGURE: [028-mass/coh713d] Difference as computed by Cohen's coefficients (probably uses another form of the formula)

FIGURE: [028-mass/mey673d] Difference as computed by Meyerhoff's coefficients (probably uses another form of the formula)

FIGURE: [028-mass/pea863d] Difference as computed by Pearson's coefficients

FIGURE: [028-mass/the053d] Difference as computed by me back in 2005

FIGURE: [028-mass/the143d] Difference as computed by wasora

Future versions of "wasora": "http://www.talador.com.ar/jeremy/wasora" may allow the inclusion of uncertainties or weighting factors associated to the experimental data, so one may pay more attention to either stable nuclei or to those nuclei whose data is more reliable. 

Of course, the coefficients reported in nuclear physics textbook may refer to other versions of Weiszacker's formula, may be fitted against another set of experimental data or whatever. Please do not understand these results as a claim that Dr.~Segre was wrong and I am not (in fact he was not!). Take this report as an example of how "wasora": "http://www.talador.com.ar/jeremy/wasora" can be used to fit non-linear multidimensional data. Actually, it is less than that. This report is a new chapter of my own scientific history.
