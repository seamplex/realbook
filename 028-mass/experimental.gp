set ticslevel 0
set xlabel "mass number A"
set ylabel "atomic number Z"
set zlabel "binding energy per nucleon [MeV]" rotate

set xrange [1:268]
set yrange [1:108]
set size ratio 108.0/268.0
unset key
set tics scale 0.2
set tics nomirror
set cbrange [0:9]

set palette defined ( 0 1 1 0.4, 8 0.7 0.7 0.8, 9 0.9 0.1 0.9)

set title "experimental binding energy per nucleon"

set terminal pdf size 16cm,9cm
set output "experimental3d.pdf"
splot "binding_per_A.dat" palette  ps 0.5 pt 57

set terminal png size 900,500 crop
set output "experimental3d.png"
replot

set terminal pdf size 16cm,9cm
set output "experimental2d.pdf"
set view map
set border 2+8

replot

set terminal png size 900,450 nocrop
set output "experimental2d.png"
replot
