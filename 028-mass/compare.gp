set ticslevel 0
set xlabel "mass number A"
set ylabel "atomic number Z"
set zlabel "binding energy per nucleon [MeV]" rotate

set xrange [1:268]
set yrange [1:108]
set size ratio 108.0/268.0
unset key
set tics scale 0.2
set tics nomirror
set cbrange [0:9]

set cbrange [-0.25:0.25]
set zrange [-0.7:0.7]
set palette defined (-0.25 0 0 1, -0.1 0.5 0.5 1 ,"0" "green", 0.1 1 0.5 0.5, 0.25 1 0 0)

set terminal pdf size 16cm,9cm
set output "xxxxx3d.pdf"
splot "xxxxx.dat" u 1:2:5 palette  ps 0.5 pt 57

set terminal png size 900,500 crop
set output "xxxxx3d.png"
replot

set terminal pdf size 16cm,9cm
set output "xxxxx2d.pdf"

set view map
set border 2+8
set title "absolute error commited by xxxxx"

replot

set terminal png size 900,450 nocrop
set output "xxxxx2d.png"
replot
