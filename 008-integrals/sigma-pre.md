The Stefan-Boltzmann constant used in every practical radiation heat transfer can be computed from other fundamental physical constants. Of course, you may think of the Stefan-Boltzmann constant as being a fundamental constant and of any other constant as depending on $\sigma$. The way you understand the universe is up to you. This example shows how you can compute it without having to resort to special mathematical tricks in order to analytically integrate a weird function---at the cost of recurring to wasora, of course.

![blackbody](blackbody.svg){.img-responsive}\ 

The energy distribution of a black body radiation was proposed by Planck in his 1900 seminal paper. The power density of a black body irradiating energy of wavelenght $\lambda$ at an absolute temperature $T$ can be written as

$$ E(\lambda,T) = \frac{2 \pi h c^2}{\displaystyle \lambda^5 \left[\exp\left(\frac{hc}{\lambda k T}\right) - 1\right]} $$

where $h$ is Planck's constant, $c$ is the speed of light in vacuum and $k$ is Boltzmann's constant. The total power irradiated at temperature $T$ is thus

$$ E_b(T) = \int_0^{\infty} \frac{2 \pi h c^2}{\displaystyle \lambda^5 \left[ \exp\left( \frac{hc}{\lambda k T}\right) - 1 \right]} \, d\lambda $$

By defining a dummy variable $\xi$

$$ \xi = \frac{\lambda k T}{h c} $$

then (work out the math as an exercise)

$$ E_b(T) = \frac{2 \pi k^4}{h^3 c^2} \int_0^{\infty} \frac{1}{\xi^5 \left[\exp\left(\xi^{-1}\right) - 1\right]} \, d\xi \, \cdot \, T^4 = \sigma \cdot T^4 $$

from which an expression for the Stefan-Bolztmann constant $\sigma$ follows. An equivalent reasoning based on frequencies instead of wavelengths throws up a similar equation. The weird integral has an analytic solution in terms of $\pi$, but I am an engineer and I do not like elliptic integrals. To avoid overflows with the evaluation of the exponential in the integrand, the integration goes from $10^{-2}$ up to $\infty$. Compare the obtained results with what Google has to say about $\sigma$. 
