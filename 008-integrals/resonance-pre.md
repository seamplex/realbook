The integral of resonance is an important parameter for computing nuclear reaction rates (see for example [Curso de Alquimia Teórica](http://www.talador.com.ar/jeremy/jaura/divagaciones/pdf/alquimia_curso.pdf). It is defined as the integral over energy

$$ I_0 = \int_{E_c}^{E_0} \frac{\sigma(E)}{E} \, dE $$

where $\sigma(E)$ is the microscopic cross section of the reaction under consideration, $E_c$ is the epithermal cut-off energy (usually $E_c = 0.5 \text{eV}$) and $E_0$ is the maximum expected energy at which the reaction can occur.

The following example computes the integral of resonance for the reaction

$$ \,^{197}\mbox{Au} + n \rightarrow \,^{198}\mbox{Au} + \gamma $$

whose microscopic cross section dependence with the incident neutron energy can be obtained online from the ENDF VII library. One accepted result is

$$ I_0 = \left( 1550 \pm 28 \right) \text{barns} $$

which should be compared with the result $I_0 = 1570$ thrown by wasora almost instantaneously, even though there are more than forty thousand energies in the microscopic cross section data.

The microscopic cross section of the reaction $^{197}\text{Au}(n,\gamma)^{198}\text{Au}$ is read from text a file which was downloaded from the [Brookhaven National Laboratory webpage](http://www.nndc.bnl.gov). It is called `au117-ng-au118.dat` and contains two columns with the energy in eV in the first one and the cross section in barns in the second one. The terminal shows the actual number of lines present, although some of these lines correspond to text headers which are already commented using the hash character `#`, which wasora ignores perfectly well. The integral of resonance is computed by using the integral functional.
