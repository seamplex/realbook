#!/bin/bash

if [ -z "`which wasora`" ]; then
  echo "wasora not found"
  exit
fi

mkdir -p html

# el dibujito de la portada
# inkscape wasorarealbook.svg -e wasorarealbook.png -C -w 550

# la version de wasora
echo "\$ wasora -v" > version.txt
wasora -v >> version.txt
echo "\$" >> version.txt


dir=$PWD

for example in `echo 0*`; do

 # los ejemplos
 out="${dir}/html/real-${example}.md"
 rm -f $out

 echo $example
 cd $example

 if [ -e meta.yaml ]; then
  title=`grep title meta.yaml | cut -d" " -f2-`
  author=`grep author meta.yaml | cut -d" " -f2-`
  tags=`grep tags meta.yaml | cut -d" " -f2-`
  difficulty=`echo $example | cut -c-3`
#   casedate=`git log -1 --pretty=format:"%ad" *`

  echo >> $out
  echo "# $title" >> $out
  echo >> $out

  if [ -e pre.sh ]; then
    ./pre.sh
  fi

  echo >> $out
  echo " - Difficulty: $difficulty/100" >> $out
  echo " - Author: $author" >> $out
#   echo " - Date: $casedate" >> $out
  echo -n " - Keywords: " >> $out
  for tag in $tags; do
    echo -n "\`$tag\`, "  >> $out
  done
  echo >> $out
  echo >> $out
  

  if [ -e intro.md ]; then
   cat intro.md >> $out
   echo >> $out
  fi

  if [ ! -e inputs ]; then
   touch inputs
  fi

  for inputorig in `cat inputs`; do
   if [ -z "$1" ]; then
     saltear=`echo $inputorig | grep \~ | wc -l`
   else
     if [[ "$example" = "`basename $1`" && -z "`echo $inputorig | grep \~`" ]]; then
       saltear=0
     else
       saltear=1
     fi
   fi
   input=`echo $inputorig | sed s/~//`
   echo "    $inputorig"

   echo >> $out
   echo "## ${input}.was" >> $out
   echo >> $out

   if [ -e $input-pre.md ]; then
     cat $input-pre.md >> $out
     echo >> $out
   fi

   echo >> $out
   echo "\`\`\`wasora" >> $out
   cat $input.was >> $out
   echo "\`\`\`" >> $out
   echo >> $out

   if [[ $saltear -eq 0 ]]  &&  [[ -e $input.sh ]]; then
    rm -f $input.term
    i=1
    n=`wc -l < $input.sh`
    while [ $i -le $n ]; do
     cat $input.sh | head -n $i | tail -n1 > tmp
     echo -n "\$ " >> $input.term
     cat tmp >> $input.term
     chmod +x tmp
     script -aq -c ./tmp $input.term 2>&1 | grep error: > errors
     if [ ! -z "`cat errors | grep -v libGL`" ]; then
      echo "error: something happened on the way to heaven"
      cat ./tmp
      cat errors
      exit 1
     fi
     i=`echo $i + 1 | bc`
    done
    echo "\$ " >> $input.term
   fi

   echo >> $out

   if [ -e $input.term ]; then

    echo >> $out
    echo "\`\`\`" >> $out

    cat $input.term | grep -v Script | sed s/^\\.\$/\$/ >> $out

    echo "\`\`\`" >> $out
    echo >> $out

   fi

   echo >> $out

   if [ -e $input.figs ]; then
    cat $input.figs | while read figure; do
     if [ -e ${figure}.svg ]; then
       cp ${figure}.svg ../html
       echo "![${figure}.svg](${figure}.svg){.img-responsive}" >> $out
     elif [ -e ${figure}.png ]; then
       cp ${figure}.png ../html
       echo "![${figure}.png](${figure}.png){.img-responsive}" >> $out
     fi
     echo >> $out
    done
   fi

   echo >> $out

   if [ -e $input-post.md ]; then
     cat $input-post.md >> $out
   fi

   echo >> $out

  done
 fi
 cd ..
done
