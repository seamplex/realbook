# videos="012-mechanics/newton.webm \
#         018-waterwheel/waterwheel.webm \
#         018-waterwheel/realwheel.webm \
#         017-double-pendulum/trajectories.webm \
#         078-boiling-channel/uniform.webm\
#         078-boiling-channel/sine.webm\
#         078-boiling-channel/arbitrary.webm"
# 
# for video in ${videos}; do
#  cp $video . || exit 
# done

cd html
cp ../wasorarealbook.svg ../*.html .
cat ../realbook.md real-*.md > full.md

# toc
rm -rf toc.html
echo "<ul class=\"nav nav-pills nav-stacked\">" >> toc.html
echo "<li style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0'><a href=\"index.html\">Introduction</a></li>" >> toc.html
for i in `ls .. | grep 0`; do
 echo -n "<li style='margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0'><a href=\"real-${i}.html\">" >> toc.html
 cat ../${i}/meta.yaml | grep title | awk -F: '{print $2}' >> toc.html
 echo "</a></li>" >> toc.html
done
echo "</ul>" >> toc.html

# index (after toc)
pandoc ../realbook.md --smart --template=markdown-template.m4.html --number-sections --mathjax -t html5 -o index.m4.html
m4 index.m4.html > index.html

# full
pandoc full.md --smart --template=markdown-template-full.m4.html --toc --toc-depth=1 --number-sections --mathjax -t html5 -o full.m4.html
sed -i 's/<!--mark--><ul>/<ul class="nav nav-pills nav-stacked"">/' full.m4.html
m4 full.m4.html > full.html

# chapters
for i in `ls .. | grep 0`; do
 echo $i
 for ext in png jpg svg webm; do
  if [ ! -z "`ls ../${i}/ | grep ${ext}`" ]; then
   cp ../${i}/*.${ext} .;
  fi
 done

 pandoc real-${i}.md --smart --template=markdown-template.m4.html --mathjax -t html5 -o real-${i}.m4.html
 m4 real-${i}.m4.html > real-${i}.html 
done

cd ..
